﻿using System;

namespace task2._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите три числа");
            Console.Write("первое = ");

            double numberOne;

            while (!double.TryParse(Console.ReadLine(), out numberOne))
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("первое = ");
            }

            Console.Write("второе = ");

            double numberTwo;

            while (!double.TryParse(Console.ReadLine(), out numberTwo))
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("второе = ");
            }

            Console.Write("третье = ");

            double numberThree;

            while (!double.TryParse(Console.ReadLine(), out numberThree))
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("второе = ");
            }

            double discriminant = (numberTwo * numberTwo) - 4 * numberOne * numberThree;

            if(discriminant < 0)
            {
                Console.WriteLine("Уравнение не имеет действительных корней");
            }
            else if (discriminant > 0)
            {
                double x1 = (-numberTwo + Math.Sqrt(discriminant)) / (2 * numberOne);
                double x2 = (-numberTwo - Math.Sqrt(discriminant)) / (2 * numberOne);
                Console.WriteLine($"Решение имеет два корня: {x1} и {x2}");
            }
            else
            {
                double x1 = (-numberTwo) / (2 * numberOne);
                Console.WriteLine($"Решение имеет один корень: {x1}");
            }
        }
    }
}
