﻿using System;

namespace task2._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите год ");
            string yearLine = Console.ReadLine();
            if(uint.TryParse(yearLine, out uint year))
            {
                if (year % 4 == 0)
                {
                    Console.WriteLine("Данный год является високосным");
                }
                else
                {
                    Console.WriteLine("Данный год НЕ является високосным");
                }
            }
            else
            {
                Console.WriteLine("Год был введён неверно.");
            }
            
            
        }
    }
}
