﻿using System;
using System.Linq;

namespace task3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите количество чисел = ");
            int length = 0;

            while (!int.TryParse(Console.ReadLine(), out length) || length<=0)
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("Введите количество чисел = ");
            }

            int[] numbers = new int[length];
            Console.WriteLine("Введите числа");

            for (int i = 0; i < length; i++)
            {
                Console.Write("{0} = ", i + 1);
                int value;

                while (!int.TryParse(Console.ReadLine(), out value))
                {
                    Console.WriteLine("При написании числа допущена ошибка");
                    Console.Write("{0} = ", i + 1);
                }

                numbers[i] = value;
            }

            int[] quantity = new int[length];

            for (int i = 0; i < length; i++)
            {
                for(int i2 = 0; i2 < length; i2++)
                {
                    if (numbers[i] == numbers[i2])
                    {
                        quantity[i]++;
                    }
                }
            }

            int index = 0;
            int maxValue = quantity.Max();

            for(int i = 0; i < length; i++)
            {
                if(numbers[i] == maxValue)
                {
                    index = i;
                }
            }
            Console.WriteLine($"Число повторяющееся большее число раз: {numbers[index]}");
        }
    }
}
