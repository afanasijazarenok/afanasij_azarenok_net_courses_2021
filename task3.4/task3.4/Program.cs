﻿using System;

namespace task3._4
{
    class Program
    {
        static void Main(string[] args)
        {
            const int Length = 20;
            const int MinValue = -5;
            const int MaxValue = 5;
            int[] numbers = new int[Length];
            Random rnd = new Random();

            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = rnd.Next(MinValue, MaxValue);
                Console.Write(numbers[i] + " ");
            }

            Console.WriteLine();
            int positiveCount = 0;
            int zeroCount = 0;
            int negativeCount = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > 0)
                {
                    positiveCount++;
                }
                else if (numbers[i] == 0)
                {
                    zeroCount++;
                }
                else
                {
                    negativeCount++;
                }
            }

            Console.WriteLine($"Количество: \n положительных: {positiveCount} \n нулей: {zeroCount} \n отрицательных: {negativeCount}");
        }
    }
}
