﻿using System;

namespace task1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите число ");
            double value;
            double.TryParse(Console.ReadLine(), out value);
            Console.WriteLine($"Ваше число {value}");
        }
    }
}
