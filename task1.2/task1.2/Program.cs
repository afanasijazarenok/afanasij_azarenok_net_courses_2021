﻿using System;

namespace task1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите число ");
            int value = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Ваше число {value}");
        }
    }
}
