﻿using System;

namespace task1._4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите 3 числа ");
            int number = Convert.ToInt32(Console.ReadLine());
            int num100 = number / 100;
            int num10 = (number/10)%10;
            int num1 = number%10;
            int answer;
            if (num100 > num10)
            {
                answer = (num100 >= num1)? num100 : num1;
            }
            else
            {
                answer = (num10 >= num1) ? num10 : num1;
            }
            Console.WriteLine($"Максимальной цифрой будет {answer}");
        }
    }
}
