﻿using System;

namespace task2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            double number1 = 0;
            double number2 = 0;
            double number3 = 0;

            Console.WriteLine("Введите три числа");
            Console.Write("первое = ");

            while (!double.TryParse(Console.ReadLine(), out number1))
            {
                Console.WriteLine("При написании числа допущена ошибка попробуйте заново");
                Console.Write("первое = ");
            }

            Console.Write("второе = ");

            while (!double.TryParse(Console.ReadLine(), out number2))
            {
                Console.WriteLine("При написании числа допущена ошибка попробуйте заново");
                Console.Write("второе = ");
            }

            Console.Write("третье = ");

            while (!double.TryParse(Console.ReadLine(), out number3))
            {
                Console.WriteLine("При написании числа допущена ошибка попробуйте заново");
                Console.Write("третье = ");
            }

            double max = (number1 > number2) ? ((number1 > number3) ? number1 : number3) : ((number2 > number3) ? number2 : number3);
            double min = (number1 < number2) ? ((number1 < number3) ? number1 : number3) : ((number2 < number3) ? number2 : number3);
            double middle = (max == number1) ? ((min == number2) ? number3 : number2) : ((max == number2) ? ((min == number1) ? number3 : number1) : ((min == number2) ? number1 : number2));

            Console.WriteLine($"По убыванию: {max} {middle} {min}");
            Console.WriteLine($"По возрастанию: {min} {middle} {max}");
            Console.WriteLine($"Минимальное: {min}");
            Console.WriteLine($"Максимальное: {max}");
            Console.WriteLine($"Число между: {middle}");
        }   
    }
}

