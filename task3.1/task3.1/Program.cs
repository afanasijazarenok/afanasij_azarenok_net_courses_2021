﻿using System;

namespace task3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите количество чисел = ");
            int length = 0;

            while (!int.TryParse(Console.ReadLine(), out length) || length<=0)
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("Введите количество чисел = ");
            }

            int[] numbers = new int[length];
            Console.WriteLine("Введите числа");

            for(int i=0; i < length; i++)
            {
                Console.Write("{0} = ", i+1);
                int value;

                while (!int.TryParse(Console.ReadLine(), out value))
                {
                    Console.WriteLine("При написании числа допущена ошибка");
                    Console.Write("{0} = ", i+1);
                }

                numbers[i] = value;
            }

            int sum = 0;

            for(int i = 0; i < length; i++)
            {
                if (numbers[i] > 0 && numbers[i] % 2 == 0)
                {
                    sum += numbers[i];
                }
            }

            Console.WriteLine($"Сумма чётных положительных чисел = {sum}");
        }
    }
}
