﻿using System;

namespace task3._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите число элементов массива = ");
            int length = 0;

            while(!int.TryParse(Console.ReadLine(), out length) || length <= 0)
            {
                Console.WriteLine("При написании числа была допущена ошибка");
                Console.Write("Введите число элементов массива = ");
            }

            int[] numbers = new int[length];

            for(int i = 0; i < length; i++)
            {
                Random rnd = new Random();
                numbers[i] = rnd.Next(-1000, 1000);
                Console.Write($"{numbers[i]} ");
            }

            Console.WriteLine();
;
            for(int i = 0; i < length-1; i++)
            {
                for(int j = 0; j < length-i-1; j++)
                {
                    if(numbers[j] > numbers[j + 1])
                    {
                        int temp;
                        temp = numbers[j];
                        numbers[j] = numbers[j + 1];
                        numbers[j + 1] = temp;

                    }
                }
            }

            for (int i = 0; i < length; i++)
            {
                Console.Write(numbers[i] + " ");
            }
        }
    }
}
