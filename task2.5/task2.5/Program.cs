﻿using System;

namespace task2._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите три числа");
            Console.Write("первое = ");

            double side1 = 0;

            while (!double.TryParse(Console.ReadLine(), out side1))
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("первое = ");
            }

            Console.Write("второе = ");

            double side2 = 0;

            while (!double.TryParse(Console.ReadLine(), out side2))
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("второе = ");
            }

            Console.Write("третье = ");

            double side3 = 0;

            while (!double.TryParse(Console.ReadLine(), out side3))
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("третье = ");
            }

            double max;
            double otherSide1, otherSide2;

            if (side1 > side2)
            {
                if (side1 > side3)
                {
                    max = side1;
                    otherSide1 = side2;
                    otherSide2 = side3;
                }
                else
                {
                    max = side3;
                    otherSide1 = side1;
                    otherSide2 = side2;
                }
            }
            else
            {
                if (side2 > side3)
                {
                    max = side2;
                    otherSide1 = side1;
                    otherSide2 = side3;
                }
                else
                {
                    max = side3;
                    otherSide1 = side1;
                    otherSide2 = side2;
                }
            }

            if (otherSide1 + otherSide2 <= max)
            {
                Console.WriteLine("Треугольника с данными сторонами не существует");
            }
            else if (otherSide1 == otherSide2 && otherSide1 == max)
            {
                Console.WriteLine("Треугольник существует и является равносторонним");
            }
            else if (otherSide1 == otherSide2 || otherSide2 == max || otherSide1 == max)
            {
                Console.WriteLine("Треугольник существует и является равнобедренным");
            }
            else
            {
                Console.WriteLine("Треугольник существует и является произвольным");
            }
        }
    }
}
