﻿using System;

namespace task2._4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите два числа");
            Console.Write("первое = ");

            int numberOne = 0;

            while (!int.TryParse(Console.ReadLine(), out numberOne))
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("первое = ");
            }

            Console.Write("второе = ");

            int numberTwo = 0;

            while (!int.TryParse(Console.ReadLine(), out numberTwo) || numberTwo == 0)
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write("второе = ");
            }

            if (numberOne % numberTwo == 0)
            {
                int divide = numberOne / numberTwo;
                Console.WriteLine("Первое число делится на второе нацело");
                Console.WriteLine($"Частное деления: {divide}");
            }
            else
            {
                Console.WriteLine("Первое число НЕ делится на второе нацело");
                int divide = numberOne / numberTwo;
                int remain = numberOne % numberTwo;
                Console.WriteLine($"Частное деления: {divide}; Остаток от деления: {remain}");
            }
        }
    }
}
