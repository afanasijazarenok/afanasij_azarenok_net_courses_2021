﻿using System;

namespace task4._0
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                const int Index1 = 1;
                const int Index2 = 2;
                Console.WriteLine("Введите два числа:");
                double numberOne = ValidateInput(Index1);
                double numberTwo = ValidateInput(Index2);
                Console.Write($"Возможные операции: \n\nСложение (1)\nВычитание (2)\nУмножение (3)\nДеление (4)\n\nВведите номер операции: ");
                Console.WriteLine($"Ответ: {CalculateAnswer(numberOne, numberTwo, Console.ReadLine())}");

                while (true)
                {
                    Console.Write("Желаете очистить операцию?\nЕсли да напшите Y(y), иначе N(n): ");
                    string solution = Console.ReadLine();

                    if (solution.ToUpper() == "Y")
                    {
                        Console.Clear();
                        break;
                    }
                    else if (solution.ToUpper() == "N")
                    {
                        Console.WriteLine();
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Неправильно введено решение");
                    }
                }
            }
        }

        enum Operations
        { 
            Add = 1,
            Subtract,
            Multiply,
            Divide,
        }

        static double ValidateInput(int index)
        {
            double numberValidated;
            Console.Write($"{index} = ");

            while (!double.TryParse(Console.ReadLine(), out numberValidated))
            {
                Console.WriteLine("При написании числа допущена ошибка");
                Console.Write($"{index} = ");
            }

            return numberValidated;
        }

        static double CalculateAnswer(double numberOne, double numberTwo, string operation)
        {
            double answer = 0;
            int validatedOperation;

            while (!int.TryParse(operation, out validatedOperation))
            {
                Console.Write("При написании операции допущена ошибка\nВведите операцию над числами: ");
                operation = Console.ReadLine();
            }
            
            switch (validatedOperation)
            {
                case (int)Operations.Add: 
                    answer = numberOne + numberTwo;
                    break;

                case (int)Operations.Subtract: 
                    answer = numberOne - numberTwo;
                    break;

                case (int)Operations.Multiply:
                    answer = numberOne * numberTwo;
                    break;

                case (int)Operations.Divide:
                    answer = numberOne / numberTwo;
                    break;
            }

            return Math.Round(answer, 2);
        }
    }
}
