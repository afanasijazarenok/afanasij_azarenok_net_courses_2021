﻿using System;

namespace task1._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите первую переменную = ");
            int valueOne = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите вторую переменную = ");
            int valueTwo = Convert.ToInt32(Console.ReadLine());
            int template;
            template = valueTwo;
            valueTwo = valueOne;
            valueOne = template;
            Console.WriteLine($"Первая переменная = {valueOne}; Вторая переменная = {valueTwo}");
        }
    }
}
